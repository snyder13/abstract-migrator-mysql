'use strict';
const { createConnection } = require('promise-mysql');

module.exports = (conf) => {
	return createConnection(conf.get('mysql'))
		.then((dbh) => new MyMigrator(dbh, conf))
		.then((migrator) => migrator.install());
};

class MyMigrator {
	constructor(dbh, { prefix }) {
		this.dbh = dbh;
		this.prefix = prefix ? `${ prefix }_` : '';
	}

	get handle() {
		return this.dbh;
	}

	// check for & maybe create log table
	async install() {
		// check for log table
		const [ { count } ] = await this.dbh.query(`SELECT count(1) AS count
			FROM information_schema.TABLES
			WHERE table_schema = database() AND table_name = ?`,
			[ `${ this.prefix }migrations` ]
		);
		if (count == 0) {
			await this.dbh.query(`CREATE TABLE ${ this.prefix }migrations (
				id serial NOT NULL PRIMARY KEY,
				name varchar(255) NOT NULL UNIQUE,
				ran timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB CHARSET=utf8`);
		}
		await this.dbh.query('BEGIN');
		return this;
	}

	// return whether we believe a migration was already applied
	async applied(name) {
		const [ { count } ] = await this.dbh.query(`SELECT COUNT(*) AS count FROM ${ this.prefix }migrations WHERE name = ?`, [ name ]);
		return count != 0;
	}

	// mark applied
	record(name) {
		return this.dbh.query(`INSERT IGNORE INTO ${ this.prefix }migrations (name) VALUES (?)`, [ name ]);
	}

	// clear applied mark
	remove(name) {
		return this.dbh.query(`DELETE FROM ${ this.prefix }migrations WHERE name = ?`, [ name ]);
	}

	commit() {
		return this.dbh.query('COMMIT');
	}

	rollback() {
		return this.dbh.query('ROLLBACK');
	}

	close() {
		return this.dbh.end();
	}
};
